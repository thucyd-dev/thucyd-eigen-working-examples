# Eigen-System Examples 

The python codes in this folder reproduce the data-driven graphs used in the January 2024 JDSA submission. The associated data appears in the `data_warehouse/eigen_decompositions/may_2023_systems/`folder. In that folder are two `json` files that record eigenvectors and -values for each day that was analyzed. 

To run the python codes that appear in this folder, make sure that you have installed the `thucyd` package (0.2.5+). The system requirements and details of how to install are available [here](https://gitlab.com/thucyd-dev/thucyd).


