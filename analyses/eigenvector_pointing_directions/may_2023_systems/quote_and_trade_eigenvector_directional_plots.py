"""
-------------------------------------------------------------------------------

This script reads in pre-computed eigen-analyses of fx panel data and plots
details related to the eigenvectors. In particular, the hemispherical and
spherical eigenvector directions as plotted as a function of mode.

-------------------------------------------------------------------------------
"""

import numpy as np
import pandas as pd
import os
import json

import matplotlib.pyplot as pmgr
from cycler import cycler
import pylab
import seaborn as sns

from thucyd import eigen

pd.options.display.width = 0
pd.options.display.precision = 2
np.set_printoptions(precision=3, linewidth=150, suppress=True)


# ----------------------------------------------------------------------------
# helpers


def participation_score(eigenvectors_: np.ndarray) -> np.ndarray:
    """Returns the participation score of the eigenvectors"""

    n_elem = eigenvectors_.shape[0]
    ipr_norms = np.sum(eigenvectors_**4, axis=0) * n_elem
    return 1 / ipr_norms


# ----------------------------------------------------------------------------
# fetch eigen json data

# defs
data_year = "2023"
data_path_src = f"../../data_warehouse/eigen_decompositions/may_2023_systems/"

# contracts here are sorted by total trade count (may 2023)
contracts = ["ec", "jy", "bp", "ad", "cd", "ne", "sf"]
contracts_to_drop = []
contracts_to_include = [c for c in contracts if c not in contracts_to_drop]

instrument = "2306"
dfsn_clk_index = 0  # using 1/2 dpx diffusion incr (with retrace suppression)

# json input file name
json_record_fn = (
    f"eigen_details.quotes-and-trades.{instrument}.{'-'.join(contracts)}."
    f"dfsn-clk-{dfsn_clk_index}.json"
)

# fetch
with open(os.path.join(data_path_src, json_record_fn), "r") as fd:
    json_data = json.load(fd)
eigen_daily_records = json_data["body"]

# setup records to cover all dates
n_dates = len(eigen_daily_records)
N = len(json_data["header"]["contracts"])


# ----------------------------------------------------------------------------
# figure setup

# font selection
pmgr.rcParams["font.family"] = "sans-serif"
pmgr.rcParams["font.sans-serif"] = ["Optima"]

# figure details
pmgr.box(False)
fig_num = 0

marker_size = 12
marker_fmt = "none"

# helper for multipane figure
n_rows_code = 200
n_cols_code = 30


# ----------------------------------------------------------------------------
# iterate over json data, compute eigenvector angles and participation scores

# thetas
thetas_px_hemi = np.zeros((N, N, n_dates))
thetas_px_sphr = np.zeros_like(thetas_px_hemi)
thetas_td_hemi = np.zeros_like(thetas_px_hemi)
thetas_td_sphr = np.zeros_like(thetas_px_hemi)

# q values
inv_qs_px = np.zeros(n_dates)
inv_qs_td = np.zeros_like(inv_qs_px)

# spectra
lambdas_px = np.zeros((N, n_dates))
lambdas_td = np.zeros_like(lambdas_px)

# participation scores
ps_px = np.zeros((N, n_dates))
ps_td = np.zeros_like(ps_px)

# iterate over eigen records
for i, eigen_daily_record in enumerate(eigen_daily_records):
    # extract records for this date
    date = eigen_daily_record["date"]
    T_px = eigen_daily_record["t_px"]
    V_px = np.array(eigen_daily_record["v_px"])
    E_px = np.array(eigen_daily_record["e_px"])
    T_td = eigen_daily_record["t_td"]
    V_td = np.array(eigen_daily_record["v_td"])
    E_td = np.array(eigen_daily_record["e_td"])

    # compute the q ratios for quotes and trades
    inv_qs_px[i] = T_px / N
    inv_qs_td[i] = T_td / N

    # record spectra
    lambdas_px[:, i] = E_px
    lambdas_td[:, i] = E_td

    # compute radii from participation score
    ps_px[:, i] = participation_score(V_px)
    ps_td[:, i] = participation_score(V_td)

    # compute hemispherical angles
    (
        V_or_px_hemi,
        E_or_px_hemi,
        signs_px_hemi,
        theta_px_hemi,
        sort_px_hemi,
    ) = eigen.orient_eigenvectors(V_px, E_px, method="arcsin")
    thetas_px_hemi[:, :, i] = theta_px_hemi

    (
        V_or_td_hemi,
        E_or_td_hemi,
        signs_td_hemi,
        theta_td_hemi,
        sort_td_hemi,
    ) = eigen.orient_eigenvectors(V_td, E_td, method="arcsin")
    thetas_td_hemi[:, :, i] = theta_td_hemi

    # compute spherical angles
    (
        V_or_px_sphr,
        E_or_px_sphr,
        signs_px_sphr,
        theta_px_sphr,
        sort_px_sphr,
    ) = eigen.orient_eigenvectors(
        V_px, E_px, method="arctan2", orient_to_first_orthant=True
    )
    thetas_px_sphr[:, :, i] = theta_px_sphr

    (
        V_or_td_sphr,
        E_or_td_sphr,
        signs_td_sphr,
        theta_td_sphr,
        sort_td_sphr,
    ) = eigen.orient_eigenvectors(
        V_td, E_td, method="arctan2", orient_to_first_orthant=True
    )
    thetas_td_sphr[:, :, i] = theta_td_sphr


# ----------------------------------------------------------------------------
# plots


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# for angular plots
def plot_eigenvector_angles(fig_num_, title_, thetas_, ps_, spherical=True):
    # setup
    N_ = thetas_.shape[0]
    n_dates_ = thetas_.shape[2]

    # common plot style for multiple datasets
    fig = pmgr.figure(num=fig_num_)
    fig.set_figwidth(9)
    fig.set_figheight(6)
    fig.suptitle(title_)

    # settings
    if spherical:
        theta_min = 0
        theta_max = 360
        rlabel_position = 135
    else:
        theta_min = -90
        theta_max = 90
        rlabel_position = 90

    # iter over modes
    for i_, mode_ in enumerate(np.arange(0, N_ - 1)):
        ax = fig.add_subplot(n_rows_code + n_cols_code + 1 + i_, projection="polar")
        ax.set_title(f"mode: {mode_}")
        ax.set_prop_cycle(cycler("color", polar_palette))

        # iter over upper triangular angles
        for j_ in np.arange(mode_ + 1, N_):
            ax.scatter(
                thetas_[mode_, j_, :],  # all dates
                ps_[mode_, :],  # all dates
                s=marker_size,
            )
        ax.set_rmax(1)
        ax.set_thetamin(theta_min)
        ax.set_thetamax(theta_max)
        if mode_ == 0:
            labels = ["0.0", None, "0.5", None, "1.0"]
        else:
            labels = [None, None, None, None, None]
        ax.set_rlabel_position(rlabel_position)
        ax.set_rgrids(
            [0, 0.25, 0.5, 0.75, 1],
            labels=labels,
        )
        ax.set_axisbelow(True)
        ax.grid(True, linewidth=0.5)

    # return the fig handle
    return fig


# setup color palette for angular plots
polar_palette = sns.color_palette("Set2", n_colors=N)
pmgr.rc("axes", prop_cycle=(cycler("color", polar_palette)))

# quote hemisphere
fig_num += 1
fig = plot_eigenvector_angles(
    fig_num,
    "quote angles -- hemispherical",
    thetas_px_hemi,
    ps_px,
    spherical=False,
)

fig_num += 1
fig = plot_eigenvector_angles(
    fig_num, "quote angles -- spherical", thetas_px_sphr, ps_px, spherical=True
)

fig_num += 1
fig = plot_eigenvector_angles(
    fig_num,
    "trade angles -- hemispherical",
    thetas_td_hemi,
    ps_td,
    spherical=False,
)

fig_num += 1
fig = plot_eigenvector_angles(
    fig_num, "trade angles -- spherical", thetas_td_sphr, ps_td, spherical=True
)
