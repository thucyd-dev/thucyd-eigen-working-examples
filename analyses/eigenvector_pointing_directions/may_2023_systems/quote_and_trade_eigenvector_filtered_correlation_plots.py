"""
-------------------------------------------------------------------------------

This script reads in pre-computed eigen-analyses performed by
`quote_and_trade_eigen_directional_filterer.py` and creates various plots
of the correlation matrices.

-------------------------------------------------------------------------------
"""

import numpy as np
import pandas as pd
import os
import json

import matplotlib.pyplot as pmgr
from matplotlib import animation
from matplotlib.animation import FFMpegWriter

import pylab
import seaborn as sns

pd.options.display.width = 0
pd.options.display.precision = 2
np.set_printoptions(precision=3, linewidth=150, suppress=True)


# ----------------------------------------------------------------------------
# helpers


def covariance_to_correlation(covariance_mtx: np.ndarray) -> tuple:
    """Transforms a covariance matrix into a correlation matrix."""

    # compute normalizing matrix
    std_devs = np.sqrt(np.diag(covariance_mtx))
    S = np.outer(std_devs, std_devs)
    S_inv = 1.0 / S

    # normalize cov to corr
    correlation_mtx = covariance_mtx * S_inv

    # return
    return correlation_mtx, std_devs


# ----------------------------------------------------------------------------
# fetch eigen json data

# defs
data_year = "2023"
data_path_src = f"../../data_warehouse/eigen_decompositions/may_2023_systems/"

# contracts here are sorted by total trade count (may 2023)
contracts = ["ec", "jy", "bp", "ad", "cd", "ne", "sf"]
contracts_to_drop = []
contracts_to_include = [c for c in contracts if c not in contracts_to_drop]

instrument = "2306"
dfsn_clk_index = 0  # using 1/2 dpx diffusion incr (with retrace suppression)

# json input file name
json_record_fn = (
    f"eigen_details.quotes-and-trades.{instrument}.{'-'.join(contracts)}."
    f"filtered.json"
)

# fetch
with open(os.path.join(data_path_src, json_record_fn), "r") as fd:
    json_data = json.load(fd)
eigen_daily_records = json_data["body"]

# setup records to cover all dates
n_dates = len(eigen_daily_records)
n_window = len(json_data["header"]["filter_weights"])
N = len(json_data["header"]["contracts"])
cursors = np.arange(n_window, n_dates)


# ----------------------------------------------------------------------------
# figure setup

# font selection
pmgr.rcParams["font.family"] = "sans-serif"
pmgr.rcParams["font.sans-serif"] = ["Optima"]

# figure details
pmgr.box(False)
fig_num = 0


# ----------------------------------------------------------------------------
# iterate over json data, compute eigenvector angles and participation scores

bands = {
    "quotes": {},
    "trades": {},
}
analyses = ["oriented", "avg_ortho", "avg_ortho_modal"]

# iter over bands
for band in bands:
    for analysis in analyses:
        # band-specific defs
        Vs = np.zeros((N, N, n_dates))
        Es = np.zeros((N, n_dates))
        Corrs = np.zeros_like(Vs)

        # iter over records (which cover unique dates)
        for i, cursor in enumerate(cursors):
            # fetch
            block = eigen_daily_records[cursor][band][analysis]
            V = np.array(block["V"])
            E = np.array(block["E"])

            # compute covariance and correlation
            Cov = V @ np.diag(E) @ V.T
            Corr = covariance_to_correlation(Cov)[0]

            # record
            Vs[:, :, cursor] = V
            Es[:, cursor] = E
            Corrs[:, :, cursor] = Corr

        # capture band-specific details
        bands[band][analysis] = {
            "Vs": Vs,
            "Es": Es,
            "Corrs": Corrs,
        }


# ----------------------------------------------------------------------------
# animation


# generator
def generate_animate_bitriptyque(axes_capture, bands_capture, analyses_capture):
    """Generates `animate_bitriptyque_fcxn` by capturing the scoped vars"""

    def animate_bitriptyque_fcxn(i_date):
        fig.suptitle("Quote and trade correlations -- from raw to eigenvector cleaned")
        for i_band, band in enumerate(bands_capture):
            for i_analysis, analysis in enumerate(analyses_capture):
                ax = axes_capture[i_band, i_analysis]
                ax.cla()
                sns.heatmap(
                    bands_capture[band][analysis]["Corrs"][:, :, 5 + i_date],
                    cmap="crest",
                    vmin=0,
                    vmax=1,
                    ax=ax,
                    cbar=False,
                )
                ax.axis("equal")
                ax.set_title(f"{band} {analysis}: {i_date + 5}")
                ax.set_xticks([])
                ax.set_yticks([])

    return animate_bitriptyque_fcxn


# kick off
# build animation of 3x2 grid where each tile is a correlation heatmap
fig_num += 1
fig, axes = pmgr.subplots(2, 3, num=fig_num, figsize=(12.0, 2 * 0.67 * 4.8))
animate_bitriptyque = generate_animate_bitriptyque(axes, bands, analyses)
animate_bitriptyque(0)

# animate
fig.ani = animation.FuncAnimation(fig, animate_bitriptyque, frames=18, repeat=False)

# save to mpg file
writer = FFMpegWriter(fps=2, metadata=dict(artist="Jay Damask"), bitrate=1800)
fig.ani.save("quotes-and-trades-correlation-cleaning.mp4", writer=writer)


# ----------------------------------------------------------------------------
# static correlation-matrix plot

# a picture of the correlation grid
fig_num += 1
fig, axes = pmgr.subplots(2, 3, num=fig_num, figsize=(12.0, 2 * 0.67 * 4.8))
animate_bitriptyque = generate_animate_bitriptyque(axes, bands, analyses)
animate_bitriptyque(0)


# ----------------------------------------------------------------------------
# plot of correlation dispersion for the three analysis types

# defs
upper_tri_indices = np.triu_indices(N, k=1)  # k=1 excludes the diagonal

# build dataframe of correlation values by analysis
"""
The final data shape looks like this:

          Date   Pair  Correlation         Analysis    Band
    0        0  e_0_1         0.27         oriented  quotes
    1        1  e_0_1         0.27         oriented  quotes
    2        2  e_0_1         0.37         oriented  quotes
        ...    ...          ...              ...     ...
    2265    15  e_5_6         0.21  avg_ortho_modal  trades
    2266    16  e_5_6         0.21  avg_ortho_modal  trades
    2267    17  e_5_6         0.20  avg_ortho_modal  trades
"""

df_tuples = []
for band in bands.keys():
    for analysis in analyses:
        # capture correlation pairs
        corr_pairs_2 = {}
        for i, j in zip(*upper_tri_indices):
            key = f"e_{i}_{j}"
            corr_pairs_2[key] = bands[band][analysis]["Corrs"][i, j, n_window:]

        # build an initial dataframe
        print(f"{band} -- {analysis}")
        df_corr_pairs = pd.DataFrame(corr_pairs_2)

        # reshape
        df_melt_corr_pairs = df_corr_pairs.reset_index().melt(
            id_vars="index", var_name="Pair", value_name="Correlation"
        )
        df_melt_corr_pairs.rename(columns={"index": "Date"}, inplace=True)
        df_melt_corr_pairs["Analysis"] = analysis
        df_melt_corr_pairs["Band"] = band

        # capture
        df_tuples.append(df_melt_corr_pairs)

# aggregate all dataframes
df_correlation = pd.concat(df_tuples).reset_index(drop=True)

# plot
fig_num += 1
fig, axes = pmgr.subplots(1, 2, num=fig_num, figsize=(12.0, 4.8))

corr_palette = "coolwarm"
for i, ax in enumerate(axes):
    this_band = list(bands.keys())[i]
    df_plot = df_correlation.query(f"Band == '{this_band}'")

    sns.stripplot(
        df_plot,
        x="Correlation",
        y="Pair",
        hue="Analysis",
        palette=corr_palette,
        legend=(i == 0),
        ax=ax,
    )
    ax.set_title(this_band)
