"""
-------------------------------------------------------------------------------

This script reads in pre-computed eigen-analyses performed by
`quote_and_trade_eigen_directional_filterer.py` and displays polar plots of
the spherical angles of the eigenvectors.

-------------------------------------------------------------------------------
"""

import numpy as np
import pandas as pd
import os
import json

import matplotlib.pyplot as pmgr
from cycler import cycler
import pylab
import seaborn as sns

from thucyd import eigen

pd.options.display.width = 0
pd.options.display.precision = 2
np.set_printoptions(precision=3, linewidth=150, suppress=True)


# ----------------------------------------------------------------------------
# helpers


def participation_score(eigenvectors_: np.ndarray) -> np.ndarray:
    """Returns the participation score of the eigenvectors"""

    n_elem = eigenvectors_.shape[0]
    ipr_norms = np.sum(eigenvectors_**4, axis=0) * n_elem
    return 1 / ipr_norms


# ----------------------------------------------------------------------------
# fetch eigen json data

# defs
data_year = "2023"
data_path_src = f"../../data_warehouse/eigen_decompositions/may_2023_systems/"

# contracts here are sorted by total trade count (may 2023)
contracts = ["ec", "jy", "bp", "ad", "cd", "ne", "sf"]
contracts_to_drop = []
contracts_to_include = [c for c in contracts if c not in contracts_to_drop]

instrument = "2306"
dfsn_clk_index = 0  # using 1/2 dpx diffusion incr (with retrace suppression)

# json input file name
json_record_fn = (
    f"eigen_details.quotes-and-trades.{instrument}.{'-'.join(contracts)}."
    f"filtered.json"
)

# fetch
with open(os.path.join(data_path_src, json_record_fn), "r") as fd:
    json_data = json.load(fd)
eigen_daily_records = json_data["body"]

# setup records to cover all dates
n_dates = len(eigen_daily_records)
n_window = len(json_data["header"]["filter_weights"])
N = len(json_data["header"]["contracts"])
cursors = np.arange(n_window, n_dates)


# ----------------------------------------------------------------------------
# figure setup

# font selection
pmgr.rcParams["font.family"] = "sans-serif"
pmgr.rcParams["font.sans-serif"] = ["Optima"]

# figure details
pmgr.box(False)
fig_num = 0

marker_size = 12
marker_fmt = "none"

# helper for multipane figure
n_rows_code = 200
n_cols_code = 30


# ----------------------------------------------------------------------------
# iterate over json data, compute eigenvector angles and participation scores

bands = {
    "quotes": {},
    "trades": {},
}
analyses = ["oriented", "avg_ortho", "avg_ortho_modal"]

# iter over bands
for band in bands:
    for analysis in analyses:
        # band-specific defs
        Vs = np.zeros((N, N, n_dates))
        Es = np.zeros((N, n_dates))
        thetas = np.zeros_like(Vs)
        p_scores = np.zeros_like(Es)

        # iter over records (which cover unique dates)
        for i, cursor in enumerate(cursors):
            # fetch
            block = eigen_daily_records[cursor][band][analysis]
            V = np.array(block["V"])
            E = np.array(block["E"])

            # compute
            _, _, _, theta, _ = eigen.orient_eigenvectors(V, E, method="arctan2")
            p_score = participation_score(V)

            # record
            Vs[:, :, cursor] = V
            thetas[:, :, cursor] = theta
            Es[:, cursor] = E
            p_scores[:, cursor] = p_score

        # capture band-specific details
        bands[band][analysis] = {
            "Vs": Vs,
            "thetas": thetas,
            "Es": Es,
            "p_scores": p_scores,
        }


# ----------------------------------------------------------------------------
# plots

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# for angular plots


def setup_polar_plot_grid(
    fig_num_: int, title_: str, width_: float = 9, height_: float = 6
) -> tuple:
    # common plot style for multiple datasets
    fig_ = pmgr.figure(num=fig_num_)
    fig_.set_figwidth(width_)
    fig_.set_figheight(height_)
    fig_.suptitle(title_)

    axes_ = []
    for i_ in range(6):
        axes_.append(
            fig_.add_subplot(n_rows_code + n_cols_code + 1 + i_, projection="polar")
        )

    return fig_, axes_


def plot_eigenvector_angles(axes_, thetas_, ps_, marker_size_, color_palette_):
    # setup
    N_ = thetas_.shape[0]

    # settings
    theta_min = 0
    theta_max = 360
    rlabel_position = 135

    # iter over modes
    for i_, mode_ in enumerate(np.arange(0, N_ - 1)):
        ax = axes_[i_]
        ax.set_prop_cycle(cycler("color", color_palette_))

        # iter over upper triangular angles
        for j_ in np.arange(mode_ + 1, N_):
            ax.scatter(
                thetas_[mode_, j_, :],  # all dates
                ps_[mode_, :],  # all dates
                s=marker_size_,
            )
        ax.set_rmax(1)
        ax.set_thetamin(theta_min)
        ax.set_thetamax(theta_max)
        if mode_ == 0:
            labels = ["0.0", None, "0.5", None, "1.0"]
        else:
            labels = [None, None, None, None, None]
        ax.set_rlabel_position(rlabel_position)
        ax.set_rgrids(
            [0, 0.25, 0.5, 0.75, 1],
            labels=labels,
        )
        ax.set_axisbelow(True)
        ax.grid(True, linewidth=0.5)


# setup color palette for angular plots
color_palette = sns.color_palette("Set2", n_colors=N)

# make plots based on (band, analysis) tuples
for band in bands:
    for analysis in analyses:
        fig_num += 1
        details = bands[band][analysis]
        fig, axes = setup_polar_plot_grid(fig_num, f"{band} angles -- {analysis}")
        plot_eigenvector_angles(
            axes,
            details["thetas"][:, :, n_window:],
            details["p_scores"][:, n_window:],
            marker_size,
            color_palette,
        )
