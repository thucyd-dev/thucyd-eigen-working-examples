"""
-------------------------------------------------------------------------------

This script reads in pre-computed eigen-analyses of fx panel data and plots
details related to the eigenvalues.

-------------------------------------------------------------------------------
"""

import numpy as np
import pandas as pd
import os
import json

import matplotlib.pyplot as pmgr
from cycler import cycler
import pylab
import seaborn as sns

pd.options.display.width = 0
pd.options.display.precision = 2
np.set_printoptions(precision=3, linewidth=150, suppress=True)


# ----------------------------------------------------------------------------
# helpers


def participation_score(eigenvectors_: np.ndarray) -> np.ndarray:
    """Returns the participation score of the eigenvectors"""

    n_elem = eigenvectors_.shape[0]
    ipr_norms = np.sum(eigenvectors_**4, axis=0) * n_elem
    return 1 / ipr_norms


def calc_mp_pdf(q_: float, l_axis_: np.ndarray, l_scale: float = 1.0) -> np.ndarray:
    """Returns the Marchenko-Pastur distribution over l-axis"""

    # set boundaries
    lam_neg = (1 - np.sqrt(q_)) ** 2
    lam_pos = (1 + np.sqrt(q_)) ** 2

    # scale the l_axis
    l_axis_scaled = l_axis_ / l_scale

    # pdf
    return (
        np.sqrt(np.maximum(0, (lam_pos - l_axis_scaled) * (l_axis_scaled - lam_neg)))
        / (2 * np.pi * q_ * l_axis_scaled)
        / l_scale
    )


# ----------------------------------------------------------------------------
# fetch eigen json data

# defs
data_year = "2023"
data_path_src = f"../../data_warehouse/eigen_decompositions/may_2023_systems/"

# contracts here are sorted by total trade count (may 2023)
contracts = ["ec", "jy", "bp", "ad", "cd", "ne", "sf"]
contracts_to_drop = []
contracts_to_include = [c for c in contracts if c not in contracts_to_drop]

instrument = "2306"
dfsn_clk_index = 0  # using 1/2 dpx diffusion incr (with retrace suppression)

# json input file name
json_record_fn = (
    f"eigen_details.quotes-and-trades.{instrument}.{'-'.join(contracts)}."
    f"dfsn-clk-{dfsn_clk_index}.json"
)

# fetch
with open(os.path.join(data_path_src, json_record_fn), "r") as fd:
    json_data = json.load(fd)
eigen_daily_records = json_data["body"]

# setup records to cover all dates
n_dates = len(eigen_daily_records)
N = len(json_data["header"]["contracts"])

# setup mode details
n_px_modes = 3  # estimated by hand
n_td_modes = 1  # estimated by hand
N_quotes = N - n_px_modes  # number of noise modes
N_trades = N - n_td_modes  # number of noise modes


# ----------------------------------------------------------------------------
# figure setup

# font selection
pmgr.rcParams["font.family"] = "sans-serif"
pmgr.rcParams["font.sans-serif"] = ["Optima"]

# figure details
pmgr.box(False)
fig_num = 0

line_width = 0.75
marker_size = 12
marker_fmt = "none"

# helper for multipane figure
n_rows_code = 200
n_cols_code = 30


# ----------------------------------------------------------------------------
# iterate over json data, compute eigenvector angles and participation scores

# q values
inv_qs = np.zeros(n_dates)
inv_qs_quotes = np.zeros(n_dates)
inv_qs_trades = np.zeros(n_dates)

# lambda axis
n_lambda = 400
lambda_axis = np.linspace(0.05, 5, n_lambda)

# spectra
lambdas_px = np.zeros((N, n_dates))
lambdas_td = np.zeros_like(lambdas_px)
lambdas_px_noise_mean = np.zeros(n_dates)
lambdas_td_noise_mean = np.zeros_like(lambdas_px_noise_mean)

# participation scores
ps_px = np.zeros((N, n_dates))
ps_td = np.zeros_like(ps_px)

# iterate over eigen records
mp_records = []
for i, eigen_daily_record in enumerate(eigen_daily_records):
    # extract records for this date
    date = eigen_daily_record["date"]
    T_px = eigen_daily_record["t_px"]
    V_px = np.array(eigen_daily_record["v_px"])
    E_px = np.array(eigen_daily_record["e_px"])
    T_td = eigen_daily_record["t_td"]
    V_td = np.array(eigen_daily_record["v_td"])
    E_td = np.array(eigen_daily_record["e_td"])

    # compute the q ratios for quotes and trades
    inv_qs[i] = T_px / N
    inv_qs_quotes[i] = T_px / N_quotes
    inv_qs_trades[i] = T_px / N_trades

    # record spectra
    lambdas_px[:, i] = E_px
    lambdas_td[:, i] = E_td

    lambdas_px_noise_mean[i] = np.mean(E_px[n_px_modes:])
    lambdas_td_noise_mean[i] = np.mean(E_td[n_td_modes:])

    # compute radii from participation score
    ps_px[:, i] = participation_score(V_px)
    ps_td[:, i] = participation_score(V_td)

    # compute MP pdfs
    mp_records.append(
        {
            "date": date,
            "l_axis": lambda_axis,
            "mp_pdf": calc_mp_pdf(1 / inv_qs[i], lambda_axis),
            "mp_pdf_quotes": calc_mp_pdf(
                1 / inv_qs_quotes[i],
                lambda_axis,
                l_scale=lambdas_px_noise_mean[i],
            ),
            "mp_pdf_trades": calc_mp_pdf(
                1 / inv_qs_trades[i],
                lambda_axis,
                l_scale=lambdas_td_noise_mean[i],
            ),
        }
    )

# create dataframe of MP distributions
df_mp = pd.concat([pd.DataFrame(d) for d in mp_records], ignore_index=True)


# ----------------------------------------------------------------------------
# plots


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# spectra plots

# setup colors
eigenval_palette = sns.color_palette("tab10", n_colors=N)
pmgr.rc("axes", prop_cycle=(cycler("color", eigenval_palette)))

# plot all on one figure
fig_num += 1
fig, axes = pmgr.subplots(1, 2, num=fig_num)
fig.set_figwidth(12)
fig.set_figheight(0.67 * 4.8)

# quotes
ax = axes[0]
for ii in range(N):
    ax.scatter(lambdas_px[ii, :], ps_px[ii, :], s=marker_size)
ax.set_ylim([0, 1])
ax.set_yticks([0, 0.5, 1])
ax.set_title(f"quotes")

ax_twin = ax.twinx()
sns.lineplot(
    data=df_mp, x="l_axis", y="mp_pdf_quotes", ax=ax_twin, linewidth=line_width
)
ax_twin.set_ylim([0, 11])
ax_twin.set_yticks([])
ax.set_xlim([0, 5])

# trades
ax = axes[1]
for ii in range(N):
    ax.scatter(lambdas_td[ii, :], ps_td[ii, :], s=marker_size)
ax.set_ylim([0, 1])
ax.set_yticks([0, 0.5, 1])

ax_twin = ax.twinx()
sns.lineplot(
    data=df_mp, x="l_axis", y="mp_pdf_trades", ax=ax_twin, linewidth=line_width
)
ax_twin.set_ylim([0, 4.5])
ax_twin.set_yticks([])
ax.set_xlim([0, 5])
ax.set_title(f"trades")
