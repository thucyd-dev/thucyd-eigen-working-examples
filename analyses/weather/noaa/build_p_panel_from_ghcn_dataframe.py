"""
-------------------------------------------------------------------------------

To be used after `build_dataframe_from_ghcn_data`, this module builds
a P-panel from raw temperature data where the columns of P are daily temp
deviations from the local average.

The resulting P-panel can be saved for subsequent analysis as well as
publication with my paper on eigenanalysis.

The typical sequence of instructions is

    df = build_panel_P(full_filename)
    ffn = save_panel_P(df)

-------------------------------------------------------------------------------
"""

import numpy as np
import pandas as pd
import os
import posixpath

from analyses.weather.noaa import build_dataframe_from_ghcn_data as df_builder

# ----------------------------------------------------------------------------
# Build and apply smooth and delay filters


def build_impulse_responses(n_days: int) -> tuple:
    """Builds triangle and ideal-delay impulse responses.

    `n_days` should be a multiple of 2 for the calculations to be lossless.
    """

    # convenience
    n_window = int(n_days / 2.0)

    # build triangle by convolving two half-length boxes
    h_box_seg = np.ones(n_window) / (n_days / 2.0)
    h_tri = np.convolve(h_box_seg, h_box_seg)

    # build ideal delay with delay = that of the triangle
    h_dly = np.zeros_like(h_tri)
    h_dly[n_window - 1] = 1.0

    # return tuple
    return h_tri, h_dly


def apply_filters_to_df(df: pd.DataFrame) -> tuple:
    """Consumes a pd df and returns a tuple of new dfs.

    Filter 1 smooths the temperature data in order to reduce the strong
    seasonality component. The smoothing filter is a triangle that spans
    several weeks.

    Filter 2 delays the raw data by the average delay of filter 1.

    The outputs of filters 1 and 2 temporally coincide. Note that the first
    N records are removed, where N = filter delay.

    The outputs are:

        yt_mtx:     (h_tri * (ts - ts[0])) + ts[0]
        yt_dly_mtx: h_dly * ts

    As an example: yt_mtx ->

        array([[19.927, -0.254, -2.178, -4.293],
               [19.554, -0.24 , -2.045, -4.278],
               [19.212, -0.226, -1.92 , -4.259],
               ..

    These values remain stored in pd dfs.
    """

    # fetch filter impulse responses
    n_days = 2 * 21
    h_tri, h_dly = build_impulse_responses(n_days)

    # make copies of the input df to hold output values
    df_ref = df.copy()
    df_dly = df.copy()

    # iter over input df columns
    for col in df:

        # apply filter 1
        cand = np.convolve(h_tri, df[col] - df[col][0]) + df[col][0]
        df_ref[col] = cand[: df.shape[0]]

        # apply filter 2
        cand = np.convolve(h_dly, df[col])
        df_dly[col] = cand[: df.shape[0]]

    # return after removing first N records
    delay = np.argmax(h_dly)
    return df_ref[delay:], df_dly[delay:]


# noinspection PyPep8Naming
def build_panel_P(full_filename: str) -> pd.DataFrame:
    """Build df panel P ~ [n-data, n-dims] from a pre-processed ghcn df.

    Sample records are

                    USC00080228  USC00098703  ...  USC00272999  USC00170814
        date                                  ...
        2010-01-21       5.0730       0.5406  ...       1.5361      -0.1068
        2010-01-22      -0.6540      -1.7546  ...       3.2474       0.3778
        2010-01-23      -4.2125      -2.9676  ...       0.0624       3.6585
        ..
    """

    # load an existing df
    df = df_builder.load_temperature_dataframe(full_filename)

    # apply filters
    df_ref, df_dly = apply_filters_to_df(df)

    # build panel P
    return df_dly - df_ref


# noinspection PyPep8Naming
def save_panel_P(df: pd.DataFrame) -> str:
    """Saves the pd df to a csv file."""

    # construct full filename
    station_slug = "{0}-stations".format(df.shape[1])
    year_slug = "{0}-{1}".format(df.index.min().year, df.index.max().year)
    fn = "P-panel.{0}.{1}.csv".format(station_slug, year_slug)
    # fmt: off
    full_filename = posixpath.join(
        os.path.dirname(__file__), df_builder.data_savepath, fn
    )
    # fmt: on

    # save and return full filename
    df.to_csv(full_filename, float_format="%-.4f")

    return full_filename


# noinspection PyPep8Naming
def load_panel_P(full_filename: str) -> pd.DataFrame:
    """Loads a previously saved P-panel into a pd df."""

    # load and return
    return pd.read_csv(full_filename, index_col="date", parse_dates=True)
