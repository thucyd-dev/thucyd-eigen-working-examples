"""
-------------------------------------------------------------------------------

Process GHCN metadata into pandas frames.

The metadata file inventory is:
    ghcnd-stations.txt
    ghcnd-countries.txt
    ghcnd-states.txt
    ghcnd-inventory.txt

These files are available at https://www1.ncdc.noaa.gov/pub/data/ghcn/daily/.
These files are in fix-width, ascii format.

From https://www1.ncdc.noaa.gov/pub/data/ghcn/daily/readme.txt:

    IV. FORMAT OF "ghcnd-stations.txt"

    ------------------------------
    Variable   Columns   Type
    ------------------------------
    ID            1-11   Character
    LATITUDE     13-20   Real
    LONGITUDE    22-30   Real
    ELEVATION    32-37   Real
    STATE        39-40   Character
    NAME         42-71   Character
    GSN FLAG     73-75   Character
    HCN/CRN FLAG 77-79   Character
    WMO ID       81-85   Character
    ------------------------------

    V. FORMAT OF "ghcnd-countries.txt"

    ------------------------------
    Variable   Columns   Type
    ------------------------------
    CODE          1-2    Character
    NAME         4-50    Character
    ------------------------------

    VI. FORMAT OF "ghcnd-states.txt"

    ------------------------------
    Variable   Columns   Type
    ------------------------------
    CODE          1-2    Character
    NAME         4-50    Character
    ------------------------------

    VII. FORMAT OF "ghcnd-inventory.txt"

    ------------------------------
    Variable   Columns   Type
    ------------------------------
    ID            1-11   Character
    LATITUDE     13-20   Real
    LONGITUDE    22-30   Real
    ELEMENT      32-35   Character
    FIRSTYEAR    37-40   Integer
    LASTYEAR     42-45   Integer
    ------------------------------

Column names are absent in these .txt files, so they will be added by this
script based on the tables above.

-------------------------------------------------------------------------------
"""

import pandas as pd
import posixpath


# ----------------------------------------------------------------------------
# Metadata files

metadata_relpath = "data_warehouse/noaa/raw/"
metadata_filename_prefix = "ghcnd"
metadata_filename_suffix = "txt"
metadata_topics = ["stations", "countries", "states", "inventory"]


# ----------------------------------------------------------------------------
# Column structures

metadata_structures = {
    "stations": {
        "colnames": [
            "id",
            "lat",
            "long",
            "elev",
            "state",
            "name",
            "gsn_flag",
            "hcn_flag",
            "wmo_id",
        ],
        "details": {
            "id": {"range": (1, 11), "dtype": "string"},
            "lat": {"range": (13, 20), "dtype": "float"},
            "long": {"range": (22, 30), "dtype": "float"},
            "elev": {"range": (32, 37), "dtype": "float"},
            "state": {"range": (39, 40), "dtype": "string"},
            "name": {"range": (42, 71), "dtype": "string"},
            "gsn_flag": {"range": (73, 75), "dtype": "string"},
            "hcn_flag": {"range": (77, 79), "dtype": "string"},
            "wmo_id": {"range": (81, 85), "dtype": "string"},
        },
    },
    "countries": {
        "colnames": ["country_code", "name"],
        "details": {
            "country_code": {"range": (1, 2), "dtype": "string"},
            "name": {"range": (4, 50), "dtype": "string"},
        },
    },
    "states": {
        "colnames": ["state_code", "name"],
        "details": {
            "state_code": {"range": (1, 2), "dtype": "string"},
            "name": {"range": (4, 50), "dtype": "string"},
        },
    },
    "inventory": {
        "colnames": ["id", "lat", "long", "elem", "year_start", "year_stop"],
        "details": {
            "id": {"range": (1, 11), "dtype": "string"},
            "lat": {"range": (13, 20), "dtype": "float"},
            "long": {"range": (22, 30), "dtype": "float"},
            "elem": {"range": (32, 35), "dtype": "string"},
            "year_start": {"range": (37, 40), "dtype": "string"},
            "year_stop": {"range": (42, 45), "dtype": "string"},
        },
    },
}


def convert_ghcn_colspec(ghcn_colspec: tuple) -> tuple:
    """Convert GHCN 1-based, inclusive colspec to np 0-based, exclusive one."""

    return ghcn_colspec[0] - 1, ghcn_colspec[1]


def convert_ghcn_colspecs(ghcn_colspecs: list) -> list:
    """Convert a list of ghcn colspecs to np colspecs."""

    return [convert_ghcn_colspec(cs) for cs in ghcn_colspecs]


def build_colspecs(metadata_structure: dict) -> list:
    """Returns a list of tuples defining the [i, j) 0-based col indices."""

    return convert_ghcn_colspecs(
        [
            metadata_structure["details"][col]["range"]
            for col in metadata_structure["colnames"]
        ]
    )


def build_coltypes(metadata_structure: dict) -> dict:
    """Returns a list of column dtypes."""

    return {
        i: metadata_structure["details"][col]["dtype"]
        for i, col in enumerate(metadata_structure["colnames"])
    }


def build_data_filename(topic: str) -> str:
    """Constructs proper filename from topic string"""

    return metadata_filename_prefix + "-" + topic + "." + metadata_filename_suffix


# ----------------------------------------------------------------------------
# State selectors

eastern_states = [
    "ME",
    "NH",
    "MA",
    "RI",
    "CT",
    "NY",
    "NJ",
    "DE",
    "MD",
    "VA",
    "NC",
    "SC",
    "GA",
    "FL",
]


# ----------------------------------------------------------------------------
# Pandas read fixed-width files


def load_metadata():

    metadata = {}

    for topic in metadata_topics:

        fn = build_data_filename(topic)
        full_filename = posixpath.join(metadata_relpath, fn)

        metadata_structure = metadata_structures[topic]
        nms = metadata_structure["colnames"]
        css = build_colspecs(metadata_structure)
        cts = build_coltypes(metadata_structure)
        print("css: {0}".format(css))
        print("cts: {0}".format(cts))

        metadata[topic] = pd.read_fwf(
            full_filename, names=nms, header=None, colspecs=css, dtypes=cts, index_col=0
        )

    return metadata


# ----------------------------------------------------------------------------
# Identify station id's that satisfy the constraints


def filter_usc_stations_for_tmax(md: dict):
    """Inventory is constrained, then joined with Stations, then constrained."""

    return (
        md["inventory"]
        .filter(regex="USC", axis=0)
        .query('elem == "TMAX"')
        .query("year_start <= 2010")
        .query("year_stop == 2020")
        .join(md["stations"][["state", "name", "hcn_flag"]])
        .dropna(subset=["hcn_flag"])
    )


def filter_from_usc_records_for_inclusive_states(df, state_list: list):
    """Constrain by list of states to include"""

    return df.set_index(["state"], append=True).query("state in {0}".format(state_list))


def filter_state_by_min_lat(df):
    """Select min lattitude by state, returning subtable with station id's.

    Produces
                        id      lat
        state
        FL     USC00080211  25.5819
        GA     USC00090140  30.7836
        SC     USC00380074  32.3953
        ..
    """

    return (
        df.reset_index()[["id", "state", "lat"]]
        .groupby(["state"])
        .min()
        .sort_values(["lat"])
    )
