"""
-------------------------------------------------------------------------------

Analyzes GHCN temperature data in n dimensions.

-------------------------------------------------------------------------------
"""

import numpy as np
import pandas as pd
import thucyd


# ----------------------------------------------------------------------------
# Analyze n-dim temperature-deviation dataset

# extraction functions for ndim return series
f_ndim_extracts = {
    "Eor": (lambda Eor, signs, sort_indices: np.diag(Eor)),
    "signs": (lambda Eor, signs, sort_indices: signs),
    "sorts": (lambda Eor, signs, sort_indices: sort_indices),
}


def analyze_ndim_timeseries(
    P: pd.DataFrame, index_details: slice, coverage: int, f_extract
) -> np.ndarray:
    """Builds an (n, P.shape[1]) matrix from orient_eigenvectors() call."""

    # setup storage mtx
    NDlen = int((index_details.stop - index_details.start) / index_details.step)
    NDwdt = P.shape[1]
    NDmtx = np.zeros((NDlen, NDwdt))

    # setup indexing
    index_starts = np.arange(
        index_details.start, index_details.stop, index_details.step
    )

    # iter across start indices
    for i, iP in enumerate(index_starts):

        # SVD
        U, SV, VT = np.linalg.svd(P[iP : iP + coverage], full_matrices=False)

        # orient
        Vor, Eor, signs, theta_mtx, sort_indices = thucyd.eigen.orient_eigenvectors(
            VT.T, np.diag(np.power(SV, 2))
        )

        # pack
        NDmtx[i, :] = f_extract(Eor, signs, sort_indices)

    # return
    return NDmtx


# extraction functions for mndim return series
f_mndim_extracts = {
    "V": (lambda V, Vor, theta_mtx: V),
    "Vor": (lambda V, Vor, theta_mtx: Vor),
    "theta": (lambda V, Vor, theta_mtx: theta_mtx.T),
}


def analyze_mndim_timeseries(
    P: pd.DataFrame, index_details: slice, coverage: int, f_extract
) -> np.ndarray:
    """Builds an (n, M, M) matrix for analysis.

    Builds an (n, P.shape[1], P.shape[1]) matrix loaded with VT.T,
    Vor, or theta_mtx, one matrix per sample, for subsequent analysis.
    """

    # setup 3D storage mtx
    NDlen = int((index_details.stop - index_details.start) / index_details.step)
    NDwdt = P.shape[1]
    MNDmtx = np.zeros((NDlen, NDwdt, NDwdt))

    # setup indexing
    index_starts = np.arange(
        index_details.start, index_details.stop, index_details.step
    )

    # iter across start indices
    for i, iP in enumerate(index_starts):

        # SVD
        U, E, VT = np.linalg.svd(P[iP : iP + coverage], full_matrices=False)

        # orient
        Vor, Eor, signs, theta_mtx, sort_indices = thucyd.eigen.orient_eigenvectors(
            VT.T, np.diag(E)
        )

        # pack
        MNDmtx[i, :] = f_extract(VT.T, Vor, theta_mtx)

    # return
    return MNDmtx


def analyze_beta_timeseries(
    PX: pd.DataFrame, Py: pd.DataFrame, index_details: slice, coverage: int
) -> np.ndarray:
    """
    Build an (n, m, 2) matrix where, for any `n`, the (m, 2) matrix
    contains (beta_uncorrected, beta_oriented).

    n = PX.shape[0]
    m = PX.shape[1]
    """

    # setup 3D storage mtx
    NDlen = int((index_details.stop - index_details.start) / index_details.step)
    Bdim = PX.shape[1]
    Bwidth = 2  # uncorrected and oriented beta vectors
    Bmtx = np.zeros((NDlen, Bdim, Bwidth))

    # setup indexing
    index_starts = np.arange(
        index_details.start, index_details.stop, index_details.step
    )

    # iter across start indices
    for i, iP in enumerate(index_starts):
        # PX and Py slices
        X = PX[iP : iP + coverage]
        y = Py[iP : iP + coverage]

        # SVD
        U, E, VT = np.linalg.svd(X, full_matrices=False)

        # orient
        Vor, Eor, signs, theta_mtx, sort_indices = thucyd.eigen.orient_eigenvectors(
            VT.T, np.diag(E)
        )

        # compose F = P x V and For = P x Vor
        F = X.dot(VT.T)
        For = X.dot(Vor)  # note Vor is derived from VT.T

        # ols for uncorrected and oriented bases
        B, res, _, _ = np.linalg.lstsq(F, y, rcond=None)
        Bor, resor, _, _ = np.linalg.lstsq(For, y, rcond=None)

        # pack
        Bmtx[i, :] = np.array([B, Bor]).T

    # return
    return Bmtx
