"""
-------------------------------------------------------------------------------

Produces an analysis in R3 using the GHCN data.

-------------------------------------------------------------------------------
"""

import numpy as np
import matplotlib.pyplot as pmgr
from mpl_toolkits import mplot3d

import os
import posixpath
from pathlib import Path

from analyses.weather.noaa import build_p_panel_from_ghcn_dataframe as p_builder
from analyses.weather.noaa import analyze_ndim_ghcn_data as analyzer


# ----------------------------------------------------------------------------
# WRT saving figures

save_figure_flag = False


def save_figure(fn: str):
    if save_figure_flag:
        filename = "produce_r3_analysis." + fn + ".eps"
        abs_path = posixpath.join(str(Path.home()), "Desktop")
        pmgr.savefig(posixpath.join(abs_path, filename), format="eps")


# ----------------------------------------------------------------------------
# Load a P dataframe from csv

file_name_eastern = "P-panel.14-stations.2010-2019.csv"
file_name_cross_us = "P-panel.3-stations.2010-2019.csv"
file_name = file_name_eastern
file_path = posixpath.join(
    os.path.dirname(__file__), p_builder.df_builder.data_savepath
)

df_P = p_builder.load_panel_P(posixpath.join(file_path, file_name))


# ----------------------------------------------------------------------------
# Call out 3 weather stations to use in this analysis

ghcn_ids_eastern = [
    "USC00080228",  # FL
    # "USC00098703",  # GA (not min lat, Tifton instead)
    # "USC00380165",  # SC
    # "USC00312635",  # NC (not min lat, Edenton instead)
    "USC00440766",  # VA
    # "USC00180700",  # MD
    # "USC00072730",  # DE
    # "USC00286055",  # NJ (not min lat, New Brunswick instead)
    # "USC00304174",  # NY (not min lat, Ithaca instead)
    # "USC00068138",  # CT (not min lat, Storrs instead)
    # "USC00374266",  # RI
    # "USC00190120",  # MA
    # "USC00272999",  # NH (not min lat, First CT Lake instead)
    "USC00170814",  # ME
]
ghcn_ids_cross_us = [
    "USC00080228",  # FL
    "USC00053005",  # CO -- FT COLLINS
    "USC00458773",  # WA -- VANCOUVER 4 NNE
]

ghcn_ids = ghcn_ids_eastern
P = df_P[ghcn_ids]

# ----------------------------------------------------------------------------
# Setup for analysis and plotting

fig_num = 0
data_slice = slice(0, 6 * 365, 1)
data_coverage = int(3 * 365)
colors = ["g", "r", "b"]


def make_3d_scatter_plot(fig, ax, data_3d):
    """Implements the details of a 3d scatter plot"""

    for i in range(3):
        ax.scatter(
            data_3d[:, 0, i],
            data_3d[:, 1, i],
            data_3d[:, 2, i],
            marker=".",
            color=colors[i],
        )

    phi = np.arange(360) * np.pi / 180.0
    ax.plot3D(
        np.cos(phi), np.sin(phi), 0.0 * np.cos(phi), color=[0.5, 0.5, 0.5], lw=0.5
    )
    ax.plot3D([-1, 1], [0, 0], [0, 0], "k", lw=0.75)
    ax.plot3D([0, 0], [-1, 1], [0, 0], "k", lw=0.75)
    ax.plot3D([0, 0], [0, 0], [-1, 1], "k", lw=0.75)

    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
    ax.set_xlim([-1, 1])
    ax.set_ylim([-1, 1])
    ax.set_zlim([-1, 1])
    ax.elev = 15
    ax.azim = 30

    pmgr.show()


# ----------------------------------------------------------------------------
# Plot delayed vs reference temp data from one station

# load ghcn data
ghcn_file_name = "ghcn-tmax-readings.14-stations.2010-2019.csv"
ghcn_df = p_builder.df_builder.load_temperature_dataframe(
    posixpath.join(file_path, ghcn_file_name)
)

# apply filters
df_ref, df_dly = p_builder.apply_filters_to_df(ghcn_df)

# plot one station
fig_num += 1
pmgr.figure(fig_num)

# fmt: off
pmgr.plot(
    df_dly["USC00440766"].loc["2012-01-01":"2015-12-31"],
    lw=0.5,
    color=[0.5, 0.5, 0.5]
)
pmgr.plot(
    df_ref["USC00440766"].loc["2012-01-01":"2015-12-31"],
    lw=0.75,
    color="b"
)
# fmt: on

pmgr.ylim([-5, 35])
pmgr.show()

save_figure("raw_data_and_smoothing")


# ----------------------------------------------------------------------------
# Plot eigenvalue timeseries to validate that there is no crossing

# eigenvalue timeseries
eval_mtx = analyzer.analyze_ndim_timeseries(
    P, data_slice, data_coverage, analyzer.f_ndim_extracts["Eor"]
)

fig_num += 1
fig, ax = pmgr.subplots(1, 1, num=fig_num)

ax.set_prop_cycle(color=colors)
ax.plot(eval_mtx / 1000, lw=0.75)
pmgr.xlim([0, 2200])
pmgr.ylim([0, 40])
pmgr.show()

save_figure("eigenvalue_ts")


# ----------------------------------------------------------------------------
# Construct and plot corrupted eigenvectors in R3

# raw eigenvector scatterplot in S2 `in` R3
evec_raw_mtx = analyzer.analyze_mndim_timeseries(
    P, data_slice, data_coverage, analyzer.f_mndim_extracts["V"]
)
fig_num += 1
fig = pmgr.figure(fig_num)
# ax = fig.add_subplot(111, projection='3d', proj_type = 'ortho')
ax = fig.add_subplot(111, projection="3d")
make_3d_scatter_plot(fig, ax, evec_raw_mtx)

save_figure("corrupted_eigenvectors")


# ----------------------------------------------------------------------------
# Construct and plot corrupted eigenvector signs in time

# time series of eigenvector signs
evec_signs = analyzer.analyze_ndim_timeseries(
    P, data_slice, data_coverage, analyzer.f_ndim_extracts["signs"]
)
fig_num += 1
fig, ax = pmgr.subplots(3, 1, num=fig_num, sharex=True, sharey=True)

for i, a in enumerate(ax):

    lf = "{0}-".format(colors[i])
    mf = "ko"

    a.stem(evec_signs[:, i], linefmt=lf, markerfmt=mf, use_line_collection=True)

ax[0].set_xlim([1000, 1024])
pmgr.show()

save_figure("corrupted_signs")


# ----------------------------------------------------------------------------
# Construct and plot oriented eigenvectors in R3

# oriented eigenvector scatterplot in S2 `in` R3
evec_oriented_mtx = analyzer.analyze_mndim_timeseries(
    P, data_slice, data_coverage, analyzer.f_mndim_extracts["Vor"]
)
fig_num += 1
fig = pmgr.figure(fig_num)
# ax = fig.add_subplot(111, projection='3d', proj_type = 'ortho')
ax = fig.add_subplot(111, projection="3d")
make_3d_scatter_plot(fig, ax, evec_oriented_mtx)

# compute mean pointing direction per axis
for i in range(3):
    mean_vec = evec_oriented_mtx[:, :, i].sum(axis=0) / evec_oriented_mtx.shape[0]
    pts = [pt for pt in zip(np.zeros(3), mean_vec)]
    ax.plot3D(pts[0], pts[1], pts[2], lw=0.5, color="b")


save_figure("oriented_eigenvectors")
