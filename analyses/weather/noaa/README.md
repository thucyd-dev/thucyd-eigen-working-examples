# Weather examples

There are two examples in this folder: one to show the correction of eigenvector signs, and the other to show an uncorrupted time series of beta weights. In both cases you only need to use the processed data that is in the warehouse.

For the eigenvector evolution example, run the code `produce_r3_analysis.py`. I recommend that you use the `P-panel.14-stations.2010-2019.csv` dataset (which is already coded).

For the beta example, run the code `produce_ols_analysis.py`. Here you will need to use the `P-panel.4-stations.2010-2019.csv` dataset because it includes Bermuda.
 
To run these examples, make sure that you have installed the `thucyd` package. The system requirements and details of how to install are available [here](https://gitlab.com/thucyd-dev/thucyd).
