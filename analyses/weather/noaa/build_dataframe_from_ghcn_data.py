"""
-------------------------------------------------------------------------------

Loads GHCN temp-max data from raw <year>.csv files.

The file contents are not directly loaded, given their size, but instead
preprocessed in bash with `ripgrep` to subselect (station_id, elem(=TMAX))
records that are then imported into a pandas dataframe.

For TMAX records, sample records look like this:

    id,date,elem,decic,<na>,<na>,<?>,<HHMM>
    USC00080228,20190101,TMAX,283,,,7,0800
    USC00080228,20190102,TMAX,283,,,7,0800

The typical sequence of instructions is

    df = build_temperature_dataframe(data_years, ghcn_ids_global)
    ffn = save_temperature_dataframe(df)

-------------------------------------------------------------------------------
"""

import pandas as pd
import io
import os
import posixpath
import subprocess


# ----------------------------------------------------------------------------
# Data files

data_relpath = "../../data_warehouse/noaa/raw/"
data_savepath = "../../data_warehouse/noaa/processed/"

data_years = [2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019]
colnames = ["id", "date", "elem", "decic", "na1", "na2", "unk", "time_str"]

# this id list started with min-lattitude-by-state and was then modified
# so that stations with relatively clean data are used.
ghcn_ids_global = [
    "USC00080228",  # FL
    "USC00098703",  # GA (not min lat, Tifton instead)
    "USC00380165",  # SC
    "USC00312635",  # NC (not min lat, Edenton instead)
    "USC00440766",  # VA
    "USC00180700",  # MD
    "USC00072730",  # DE
    "USC00286055",  # NJ (not min lat, New Brunswick instead)
    "USC00304174",  # NY (not min lat, Ithaca instead)
    "USC00068138",  # CT (not min lat, Storrs instead)
    "USC00374266",  # RI
    "USC00190120",  # MA
    "USC00272999",  # NH (not min lat, First CT Lake instead)
    "USC00170814",  # ME
]

ghcn_ids_cross_us = [
    "USC00080228",  # FL
    "USC00053005",  # CO -- FT COLLINS
    "USC00458773",  # WA -- VANCOUVER 4 NNE
]

ghcn_ids_east_coast_plus_bd = [
    "USC00080228",  # FL
    "USC00440766",  # VA
    "USC00170814",  # ME
    "BDM00078016",  # Bermuda
]


# ----------------------------------------------------------------------------
# Preprocessed csv read


def import_data_by_year_ghcnid(year: int, ghcn_id: str) -> pd.DataFrame:
    """Imports requested records from NOAA files into a pd df.

    Starts with raw csv records like

        US1FLSL0019,20190101,PRCP,0,,,N,
        US1NVNY0012,20190101,SNOW,0,,,N,
        US1ILWM0012,20190101,PRCP,163,,,N,
        ..

    uses `rg` to build records like

        USC00080228,20150101,TMAX,217,,,7,0800
        USC00080228,20150102,TMAX,256,,,7,0800
        USC00080228,20150103,TMAX,272,,,7,0800
        ..

    and then returns a df like

                    USC00080228
        date
        2015-01-01         21.7
        2015-01-02         25.6
        2015-01-03         27.2
        ..

    From this, other ghcn_ids can be union-joined (w/in the same year).
    """

    # manage read from disk
    fn = "{0}".format(year) + ".csv"
    full_filename = posixpath.join(os.path.dirname(__file__), data_relpath, fn)

    # use `ripgrep` (`rg` on the cli) to filter for the ghdn_id
    # fmt: off
    rg_by_id = subprocess.\
        Popen(["rg", ghcn_id, full_filename], stdout=subprocess.PIPE)
    # fmt: on

    # pipe to another `rg` call to search for `TMAX`
    rg_id_with_tmax = subprocess.Popen(
        ["rg", "TMAX"], stdin=rg_by_id.stdout, stdout=subprocess.PIPE
    )

    # pull back the stdout text, decode, and load into a string-io buff
    data_str = (rg_id_with_tmax.communicate()[0]).decode("utf-8")
    data_buf = io.StringIO(data_str)

    # import buffer into dataframe
    df = pd.read_csv(
        data_buf, header=None, names=colnames, index_col=False, parse_dates=[1]
    )

    # convert temp in to degrees-c, set new column with colname=id
    df[ghcn_id] = df.decic / 10.0

    # index by date and return the ghcn_id column only
    return df.set_index(["date"])[[ghcn_id]]


# ----------------------------------------------------------------------------
# Concatenations: multi-year data for same ghcn-id, then multi-ghcn-ids


def import_data_by_ghcnid(years: list, ghcn_id: str) -> pd.DataFrame:
    """Combines pd df `date` index across multiple years."""

    # build up list of df's
    dfs = [import_data_by_year_ghcnid(year, ghcn_id) for year in years]

    # concat along the index so that we have all dates, and return
    return pd.concat(dfs, axis=0)


def build_temperature_dataframe(years: list, ghcn_ids: list) -> pd.DataFrame:
    """Build of union-join on dates across ghcn-ids.

    As an example,
                    USC00080228  USC00440766  USC00068138  USC00170814
        date
        2010-01-01         25.0          1.1         -2.8         -4.4
        2010-01-02         18.9          1.7          1.7         -3.9
        ..

    The returned df is cleaned of NaN values.
    """

    dfs = [import_data_by_ghcnid(years, ghcn_id) for ghcn_id in ghcn_ids]

    return pd.concat(dfs, axis=1, join="outer").dropna()


def save_temperature_dataframe(df: pd.DataFrame) -> str:
    """Saves df to 'ghcn-tmax-readings.<y1>-<y2>.<#>-stations.csv."""

    # construct full filename
    station_slug = "{0}-stations".format(df.shape[1])
    year_slug = "{0}-{1}".format(df.index.min().year, df.index.max().year)
    fn = "ghcn-tmax-readings.{0}.{1}.csv".format(station_slug, year_slug)
    full_filename = posixpath.join(os.path.dirname(__file__), data_savepath, fn)

    # save and return full filename
    df.to_csv(full_filename)

    return full_filename


def load_temperature_dataframe(full_filename: str) -> pd.DataFrame:
    """Loads a pre-processed dataframe-file into a pd df."""

    # load and return
    return pd.read_csv(full_filename, index_col="date", parse_dates=True)


# ----------------------------------------------------------------------------
# Development tools


def scan_noaa_data(ghcn_ids: list):

    # iter on ids and call out # records / year
    for ghcn_id in ghcn_ids:

        print("id: {0}".format(ghcn_id))

        for year in data_years:

            df = import_data_by_year_ghcnid(year, ghcn_id)

            print("  yr: {0}, cnt: {1}".format(year, df.shape[0]))


def scan_noaa_site(ghcn_id: str):

    print("id: {0}".format(ghcn_id))

    for year in data_years:

        df = import_data_by_year_ghcnid(year, ghcn_id)

        print("  yr: {0}, cnt: {1}".format(year, df.shape[0]))
