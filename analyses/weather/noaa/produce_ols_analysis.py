"""
-------------------------------------------------------------------------------

Produces a regression analysis y = X beta + eps with GHCN data.

X = data in R^3, the same stations used in `produce_r3_analysis.py` but
with fewer records.

y = data in R^1, the Bermuda station.

beta's are uncorrected and oriented.

-------------------------------------------------------------------------------
"""

import numpy as np
import matplotlib.pyplot as pmgr

import os
import posixpath
from pathlib import Path

from analyses.weather.noaa import build_p_panel_from_ghcn_dataframe as p_builder
from analyses.weather.noaa import analyze_ndim_ghcn_data as analyzer


# ----------------------------------------------------------------------------
# WRT saving figures

save_figure_flag = True


def save_figure(fn: str):
    if save_figure_flag:
        filename = "produce_ols_analysis." + fn + ".eps"
        abs_path = posixpath.join(str(Path.home()), "Desktop")
        pmgr.savefig(posixpath.join(abs_path, filename), format="eps")


# ----------------------------------------------------------------------------
# Load a P dataframe from csv

file_name_east_coast_plus_bd = "P-panel.4-stations.2010-2019.csv"
file_name = file_name_east_coast_plus_bd
file_path = posixpath.join(
    os.path.dirname(__file__), p_builder.df_builder.data_savepath
)

df_P = p_builder.load_panel_P(posixpath.join(file_path, file_name))


# ----------------------------------------------------------------------------
# Call out 4 weather stations to use in this analysis

ghcn_ids_east_coast = [
    "USC00080228",  # FL
    "USC00440766",  # VA
    "USC00170814",  # ME
]

ghcn_ids_bermuda = [
    "BDM00078016",  # Bermuda
]

PX = df_P[ghcn_ids_east_coast]
Py = df_P[ghcn_ids_bermuda]


# ----------------------------------------------------------------------------
# Setup for analysis and plotting

fig_num = 0
data_slice = slice(0, 6 * 250, 1)  # there are ~250 rec / yr for Bermuda
data_coverage = int(3 * 250)
colors = ["g", "r", "b"]


# ----------------------------------------------------------------------------
# Plot delayed vs reference temp data from the Bermuda station

# load ghcn data
ghcn_file_name = "ghcn-tmax-readings.4-stations.2010-2019.csv"
ghcn_df = p_builder.df_builder.load_temperature_dataframe(
    posixpath.join(file_path, ghcn_file_name)
)

# apply filters
df_ref, df_dly = p_builder.apply_filters_to_df(ghcn_df)

# plot one station
fig_num += 1
pmgr.figure(fig_num)

# fmt: off
pmgr.plot(
    df_dly["BDM00078016"].loc["2012-01-01":"2015-12-31"],
    lw=0.5,
    color=[0.5, 0.5, 0.5]
)
pmgr.plot(
    df_ref["BDM00078016"].loc["2012-01-01":"2015-12-31"],
    lw=0.75,
    color="b"
)
# fmt: on

pmgr.ylim([-5, 35])
pmgr.show()
save_figure("bermuda_raw_data_and_smoothing")


# ----------------------------------------------------------------------------
# Plot uncorrected and oriented betas over a time-series slice

fig_num += 1
fig, ax = pmgr.subplots(2, 1, num=fig_num, sharex=True, sharey=True)

# compute beta-compare time series
beta_compare = analyzer.analyze_beta_timeseries(PX, Py, data_slice, data_coverage)

# iter on beta type
for i, a in enumerate(ax):

    betas = beta_compare[:, :, i]

    # iter on beta element
    for j, b in enumerate(betas.T):

        a.plot(b, ".", color=colors[j], ms=0.5)

pmgr.xlim([0, 1500])
pmgr.ylim([-0.1, 0.1])
pmgr.show()

save_figure("beta_series_corrupted_and_corrected")
