# Download GHCN CSV Data

The original GHCN data is too large to duplicate in this repo, but the data can be found online. This repo includes data that was processed from the raw data; the relpath is `../processed/`. 

For **raw** data, start from the landing page for the GHCN data is [here](https://www.ncdc.noaa.gov/data-access/land-based-station-data/land-based-datasets/global-historical-climatology-network-ghcn). From here, you can navigate to their [public drive](ftp://ftp.ncdc.noaa.gov/pub/data/ghcn/daily/). 

The data you should [download](ftp://ftp.ncdc.noaa.gov/pub/data/ghcn/daily/by_year/) is in `by_year/`, with filenames such as `2010.csv.gz`, `2011.csv.gz`, etc. The processed data consumes raw data from 2010-2019. Each file is about 1.5GB in size.

