<img src="https://gitlab.com/thucyd-dev/thucyd/raw/master/images/thucyd-tile-logo.1500px.png" alt="thucyd-logo" height="150"> 


# Data and Code for Eigenanalysis Demonstrations
          
[![license](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://gitlab.com/thucyd-dev/thucyd/blob/master/LICENSE)
[![python versions](https://img.shields.io/pypi/pyversions/thucyd.svg)](https://pypi.python.org/pypi/thucyd)
[![numpy versions](https://img.shields.io/badge/numpy-%3E%3D1.23-blue.svg)](https://www.numpy.org/)
[![pypi version](https://img.shields.io/pypi/v/thucyd.svg)](https://pypi.python.org/pypi/thucyd)
[![Conda Version](https://img.shields.io/conda/vn/conda-forge/thucyd.svg)](https://anaconda.org/conda-forge/thucyd)
[![Python Black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black) 

## Purpose

The purpose of this repo is to publicly store code and data that helps to demonstrate aspects of the `thucyd` Python package. 

## What is in the repo

This code in this repo is not library code but user code. Therefore, there are no unit tests nor coverage. 

The user may have to work through environment variables or absolute paths in order to have the code work. However, I expect that once these typical environment issues are settled that the code should work as advertised. 

## JDSA Readers

To readers of the International Journal of Data Science and Analytics, this repo relates to two papers. 

For the [first paper](https://link.springer.com/article/10.1007/s41060-020-00227-z), of July 2020, navigate to the `analyses/weather/noaa/` dir, read the `README.md` file there, and run the files named `produce_r3_analysis.py` and `produce_ols_analysis.py` with Python.

For the second paper (pending), of January 2024, navigate to the `analyses/eigenvector_pointing_directions/may_2023_systems/` dir and read the `README.md` file there. The Python files in that path will reproduce the data-centric figures that appear in the paper. 

## Buell Lane Press

[Buell Lane Press](https://buell-lane-press.co) is the package sponsor. 


